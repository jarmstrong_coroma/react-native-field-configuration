import React, { Component } from 'react';
import { View, Text, TextInput, StyleSheet } from 'react-native';

const TEXT_REGEX = /^Text\s*\((\d*)\)/i;

export default class TextField extends Component {
  render() {
    const { Displayformat, Label } = this.props.json;
    const maxLength = this.getMaxLength(Displayformat);

    return (
      <View>
        <Text>{Label}</Text>
        <TextInput
          editable={true}
          maxLength={maxLength}
         />
      </View>
    )
  }

  getMaxLength(displayFormat) {
    var sizeMatch = TEXT_REGEX.exec(displayFormat);

    if (sizeMatch !== null && sizeMatch.length > 1) {
      return parseFloat(sizeMatch[1]);
    }

    return 0;
  }
}
