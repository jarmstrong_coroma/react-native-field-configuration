import React, { Component } from 'react';
import { View, Text } from 'react-native';
import Checkbox from 'react-native-check-box';
import GridView from 'react-native-super-grid';

export default class CheckboxGrid extends Component {
  render() {
    const { Span, Label, SubComponents } = this.props.json;

    return (
      <View>
        <Text>{Label}</Text>
        <GridView
          // itemDimension={130}
          items={SubComponents}
          renderItem={item => (
            <Checkbox
              style={{flex: 1, padding: 10}}
              leftText={item.Label}
              onClick={() => {}}
            />)}
         />
      </View>
    );
  };
}
