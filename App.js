import React from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import _ from 'underscore';

import config from './data/example.json';
import TextField from './src/components/TextField';
import CheckboxGrid from './src/components/CheckboxGrid';

const TEXT_REGEX = /^Text\s*\((\d*)\)/i;
const TEXT_BOX_REGEX = /^Text\s*\(Box\)/i;

export default class App extends React.Component {
  render() {
    var components = config["Components"];

    var textField = _.find(components, (c) => {
      return TEXT_REGEX.test(c["Displayformat"]);
    });

    var checkBoxGrid = _.find(components, (c) => {
      return c["Displayformat"].toUpperCase() === "Sub-Components as Checkboxes".toUpperCase();
    });

    return (
      <View style={styles.container}>
        <Text style={{fontWeight: 'bold'}}>{textField["Displayformat"]}</Text>
        <TextField json={textField} />

        <Text style={{fontWeight: 'bold'}}>{checkBoxGrid["Displayformat"]}</Text>
        <CheckboxGrid json={checkBoxGrid} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
